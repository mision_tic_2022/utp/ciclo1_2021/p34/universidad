#Clase que me representa la interfaz de usuario en consola
from controlador.controlador import Controlador


class InterfazConsola:
    #Método constructor
    def __init__(self) -> None:
        self.controlador: Controlador = Controlador()
        self.menu()
    
    def menu(self):
        opc = 0
        while opc != -1:
            print("1 --> Matricular estudiante")
            print("2 --> Consultar estudiante")
            print("3 --> Mostrar todos los estudiantes")
            print("-1 --> Salir")
            opc = int(input("Por favor ingrese una opción: "))
            if opc == 1:
                self.controlador.matricular_estudiante("juan", "herrera", "1234", "m")
            elif opc == 2:
                obj_estudiante = self.controlador.get_universidad().buscar_estudiante("1234")
                print(obj_estudiante)



"""
1 --> Matricular estudiante
2 --> Consultar estudiante
3 --> Mostrar todos los estudiantes
-1 --> Salir
"""