#importar los elementos de tkinter
from tkinter import *

#construir una ventana
window = Tk()
#Cambiar el titulo de la ventana
window.title("Mi primera ventana")
#Crear una etiqueta para 'nombre'
lbl_nombre = Label(window, text="Nombre: ")
#Ubicar el elemento lbl_nombre en la ventana
lbl_nombre.grid(column=0, row=0)
#Campo de texto para el nombre
entry_nombre = Entry(window, width=15)
#Ubicar el campo de texto en pantalla
entry_nombre.grid(row=0, column=1)
#Crear etiqueta para el apellido
lbl_apellido = Label(window, text="Apellido: ")
#Ubicar la etiqueta en la ventana
lbl_apellido.grid(column=0, row=1)
#Crear campo para el apellido
entry_apellido = Entry(window, width=15)
#Ubicar el campo de texto en la ventana
entry_apellido.grid(column=1, row=1)
#Función manejadora de eventos del botón Guardar
def click():
    print("Click!")
    print("Nombre: "+entry_nombre.get())
    print("Apellido: "+entry_apellido.get())
    entry_nombre.delete(0, 'end')
    entry_apellido.delete(0, 'end')
#Crear botón guardar
btn_guardar = Button(window, text="Guardar", command=click)
btn_guardar.grid(row=2, column=1)

#Tamaño de la ventana
window.geometry("300x200")
#Mostrar ventana
window.mainloop()

