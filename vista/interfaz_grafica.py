from tkinter import *
#Importa los widgets
from tkinter.ttk import *
from controlador.controlador import Controlador

#Clase que me representa la interfaz gráfica
class InterfazGrafica:
    #Método constructor
    def __init__(self) -> None:
        self.controlador = Controlador()
        self.index = 0
        self.uiid = 0
        #Crear ventana
        self.window = Tk()
        #Añadir titulo a la ventana
        self.window.title("Software universitario")
        self.crear_form_estudiante()
        self.crear_tabla_estudiantes()
        self.mostrar_ventana()
    
    #Método para crear el formulario del estudiante
    def crear_form_estudiante(self):
        self.lbl_nombre = Label(self.window, text="Nombre: ")
        self.entry_nombre = Entry(self.window, width=15)
        self.lbl_apellido = Label(self.window, text="Apellido: ")
        self.entry_apellido = Entry(self.window, width=15)
        self.lbl_cedula = Label(self.window, text="Cédula: ")
        self.entry_cedula = Entry(self.window, width=15)
        self.lbl_sexo = Label(self.window, text="Sexo: ")
        self.entry_sexo = Entry(self.window, width=15)
        self.btn_matricular_estudiante = Button(self.window, text="Matricular estudiante", command=self.matricular_estudiante)

    #Método para crear la tabla de los estudiantes
    def crear_tabla_estudiantes(self):
        self.tabla_estudiantes = Treeview(self.window, columns=4)
        self.tabla_estudiantes["columns"] = ["nombre", "apellido", "cedula", "sexo"]
        self.tabla_estudiantes["show"] = "headings"
        self.tabla_estudiantes.heading("nombre", text="Nombre")
        self.tabla_estudiantes.heading("apellido", text="Apellido")
        self.tabla_estudiantes.heading("cedula", text="Cédula")
        self.tabla_estudiantes.heading("sexo", text="Sexo")

    """
    Método para ubicar los elementos en pantalla y mostrar la
    ventana principal
    """
    def mostrar_ventana(self):
        self.lbl_nombre.grid(row=0, column=0)
        self.entry_nombre.grid(row=0, column=1)
        self.lbl_apellido.grid(row=1, column=0)
        self.entry_apellido.grid(row=1, column=1)
        self.lbl_cedula.grid(row=2, column=0)
        self.entry_cedula.grid(row=2, column=1)
        self.lbl_sexo.grid(row=3, column=0)
        self.entry_sexo.grid(row=3, column=1)
        self.btn_matricular_estudiante.grid(row=4, column=1)
        self.tabla_estudiantes.place(x=10, y=140)
        #Tamaño de la ventana
        self.window.geometry("850x500")
        #Mostrar ventana
        self.window.mainloop()

    #Manejador de eventos del botón
    def matricular_estudiante(self):
        #Obtenemos los datos de los campos de texto
        nombre = self.entry_nombre.get()
        apellido = self.entry_apellido.get()
        cedula = self.entry_cedula.get()
        sexo = self.entry_sexo.get()
        #Crear un objeto estudiante y matricularlo en universidad
        self.controlador.matricular_estudiante(nombre, apellido, cedula, sexo)
        #Insertar un nuevo registro en la "tabla"
        tupla_estudiante = (nombre, apellido, cedula, sexo)
        self.tabla_estudiantes.insert('', self.index, self.uiid, values=tupla_estudiante)
        self.index += 1
        self.uiid += 1
        self.limpiar_formulario()
    
    def limpiar_formulario(self):
        self.entry_nombre.delete(0, 'end')
        self.entry_apellido.delete(0, 'end')
        self.entry_cedula.delete(0, 'end')
        self.entry_sexo.delete(0, 'end')






