#Importa la clase Estudiante
from modelo.estudiante import Estudiante

#Clase que me representa a una universidad
class Universidad:
    #Método constructor
    def __init__(self, nombre: str) -> None:
        self.nombre = nombre
        self.estudiantes: list[Estudiante] = []
    
    """
    Métodos consultores
    """
    def get_nombre(self)->str:
        return self.nombre
    
    def get_estudiantes(self)->list:
        return self.estudiantes
    
    """
    Métodos modificadores
    """
    def set_nombre(self, nombre: str):
        self.nombre = nombre
    
    def set_estudiantes(self, estudiantes: list):
        self.estudiantes = estudiantes
    
    #Acciones
    def matricular_estudiante(self, estudiante: Estudiante):
        self.estudiantes.append(estudiante)
    
    def buscar_estudiante(self, cedula: str)->Estudiante:
        filtro_est = list(filter(lambda est: est.get_cedula() == cedula, self.estudiantes))
        return filtro_est[0]
    
    
    