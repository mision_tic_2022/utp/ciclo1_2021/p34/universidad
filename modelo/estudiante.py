#Clase que me representa un estudiante
class Estudiante:
    #Método constructor
    def __init__(self, nombre: str, apellido: str, cedula: str, genero: str) -> None:
        self.nombre = nombre
        self.apellido = apellido
        self._cedula = cedula
        self.genero = genero
    
    #Método que convierte la clase a string
    def __str__(self) -> str:
        str_estudiante: str = "Nombre: "+self.nombre+"\n"
        str_estudiante += "Apellido: "+self.apellido+"\n"
        str_estudiante += "Cédula: "+self._cedula+"\n"
        str_estudiante += "Género: "+self.genero+"\n"
        return str_estudiante
    
    """
    Métodos consultores
    Getters
    """
    def get_nombre(self)->str:
        return self.nombre
    
    def get_apellido(self)->str:
        return self.apellido
    
    def get_cedula(self)->str:
        return self._cedula
    
    def get_genero(self)->str:
        return self.genero

    """
    Métodos modificadores
    Setters
    """
    def set_nombre(self, nombre: str):
        self.nombre = nombre
    
    def set_apellido(self, apellido: str):
        self.apellido = apellido
    
    def set_genero(self, genero: str):
        self.genero = genero

#obj_estudiante = Estudiante("Martha", "Perea", "12345", "F")
#print(obj_estudiante)