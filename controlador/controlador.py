
from modelo.estudiante import Estudiante
from modelo.universidad import Universidad

#Clase que representa un controlador
class Controlador:
    #Método constructor
    def __init__(self) -> None:
        self.universidad: Universidad = Universidad("UTP")
    
    #Método consultor
    def get_universidad(self)->Universidad:
        return self.universidad
    
    def matricular_estudiante(self, nombre, apellido, cedula, genero):
        #Crear objeto estudiante
        obj_estudiante: Estudiante = Estudiante(nombre, apellido, cedula, genero)
        #Matricular al estudiante creado en la universidad
        self.universidad.matricular_estudiante(obj_estudiante)
    
    